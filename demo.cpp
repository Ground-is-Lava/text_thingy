#include <fstream>
#include <iostream>
#include <sstream>

#include "glad.h"
#include <GLFW/glfw3.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "text_thingy.hpp"

using std::cerr;
using std::cout;
using std::string;
using std::u32string;

static GLuint make_program();
static GLuint make_shader(const char* path, GLenum type);
u32string utf8_to_utf32(const string&);

int main(int argc, char** argv)
{
	string font_path = "Anonymous Pro/Anonymous Pro.ttf";
	if(argc > 1)
	{
		font_path = argv[1];
	}

	if(!glfwInit())
	{
		cerr << "glfwInit() failed\n";
		return 1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	const int width = 640;
	const int height = 480;
	GLFWwindow* window = glfwCreateWindow(width, height, "text_thingy", nullptr, nullptr);
	if(window == nullptr)
	{
		cerr << "error creating window\n";
		return 1;
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1); // enable vsync

	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	glfwSetWindowPos(window, (mode->width - width) / 2, (mode->height - height) / 2);

	if(!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
	{
		cerr << "error loading glad\n";
		return 1;
	}
	cout << "OpenGL " << GLVersion.major << "." << GLVersion.minor << " loaded\n";

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	const GLuint shader = make_program();
	if(shader == 0)
	{
		return 1;
	}

	const float widthf = width;
	const float heightf = height;
	glm::mat4 projection_matrix = glm::ortho(0.0f, widthf, heightf, 0.0f, -1.0f, 1.0f);
	const GLuint s_projection = glGetUniformLocation(shader, "projection");
	glUseProgram(shader);
	glUniformMatrix4fv(s_projection, 1, GL_FALSE, glm::value_ptr(projection_matrix));
	text_thingy text(font_path, 24, shader);

	static u32string prompt = utf8_to_utf32("C:\\>");
	static u32string s = prompt;
	glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if(action == GLFW_RELEASE)
		{
			return;
		}

		if(key == GLFW_KEY_ESCAPE)
		{
			glfwSetWindowShouldClose(window, GL_TRUE);
		}
		else if(key == GLFW_KEY_ENTER)
		{
			(s += '\n') += prompt;
		}
		else if(key == GLFW_KEY_TAB)
		{
			s += '\t';
		}
		else if(key == GLFW_KEY_BACKSPACE)
		{
			if(s.length() > 0)
			{
				s.pop_back();
			}
		}
	});
	glfwSetCharCallback(window, [](GLFWwindow* window, unsigned int codepoint)
	{
		s += static_cast<char32_t>(codepoint);
	});

	while(!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT);

		text.draw(s + static_cast<char32_t>('|'), {8, 8});

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	return 0;
}

static GLuint make_program()
{
	const GLuint vs = make_shader("text.vs", GL_VERTEX_SHADER);
	if(vs == 0) return 0;
	const GLuint fs = make_shader("text.fs", GL_FRAGMENT_SHADER);
	if(fs == 0) return 0;

	const GLuint program = glCreateProgram();

	glAttachShader(program, vs);
	glAttachShader(program, fs);

	glLinkProgram(program);

	glDetachShader(program, vs); glDeleteShader(vs);
	glDetachShader(program, fs); glDeleteShader(fs);

	return program;
}

static GLuint make_shader(const char* path, const GLenum type)
{
	std::ifstream f(path);
	std::stringstream ss;
	ss << f.rdbuf();
	const string source = ss.str();
	const char* source_c = source.c_str();
	GLint source_len = static_cast<GLint>(source.length());

	const GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &source_c, &source_len);
	glCompileShader(shader);

	GLint compile_ok;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_ok);
	if(compile_ok == GL_FALSE)
	{
		cerr << "error compiling " << path << "\n";
		return 0;
	}

	return shader;
}
