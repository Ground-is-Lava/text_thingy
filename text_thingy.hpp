#pragma once

#include <stdint.h>
#include <string>
#include <unordered_map>

#include "glad.h"

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

class text_thingy
{
	public:
		text_thingy(const std::string& font_path, FT_UInt height, GLuint shader);
		~text_thingy();

		void draw(const std::string&, const glm::dvec2& pos);
		void draw(const std::u32string&, glm::dvec2 pos);

		struct Character
		{
			GLuint		texture;
			glm::ivec2	size;
			glm::ivec2	bearing;	// offset from baseline to left/top of glyph
			double		x_offset;
			bool		flip;
		};

		static const uint8_t tab_width = 4;

	private:
		FT_Library ft;
		FT_Face face;
		FT_UInt height;
		std::unordered_map<char, Character> chars;
		GLuint vbo;
		GLuint vao;
		GLuint shader;
		GLint s_color;

		Character& get_char(char32_t);
};
