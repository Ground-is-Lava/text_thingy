#include "text_thingy.hpp"

#include <iostream>
#include <stdint.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/vec3.hpp>

using std::cerr;
using std::string;
using std::u32string;

// based on:
// http://learnopengl.com/#!In-Practice/Text-Rendering
// http://learnopengl.com/#!In-Practice/2D-Game/Render-text

text_thingy::Character load_char(const FT_Face& face, const FT_ULong c);
u32string utf8_to_utf32(const string&);

text_thingy::text_thingy(const std::string& font_path, const FT_UInt height, const GLuint shader)
:
	height(height),
	shader(shader)
{
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	s_color = glGetUniformLocation(shader, "color");

	glm::vec3 color(1.0);
	glUseProgram(shader);
	glUniform3fv(s_color, 1, glm::value_ptr(color));

	if(FT_Init_FreeType(&ft))
	{
		throw std::runtime_error("failed to init FreeType");
	}

	if(FT_New_Face(ft, font_path.c_str(), 0, &face))
	{
		throw std::runtime_error("failed to load font " + font_path);
	}

	FT_Set_Pixel_Sizes(face, 0, height);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	chars.emplace(' ', load_char(face, ' '));
	chars.emplace('H', load_char(face, 'H'));
}

text_thingy::~text_thingy()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	FT_Done_Face(face);
	FT_Done_FreeType(ft);
}

void text_thingy::draw(const string& s, const glm::dvec2& pos)
{
	draw(utf8_to_utf32(s), pos);
}

void text_thingy::draw(const u32string& s, glm::dvec2 pos)
{
	const double start_x = pos.x;

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glUseProgram(shader);

	glActiveTexture(GL_TEXTURE0);

	uint_fast32_t line_i = 0;
	for(const char32_t c : s)
	{
		if(c == '\n')
		{
			pos.x = start_x;
			pos.y += height;
			line_i = 0;
			continue;
		}
		if(c == '\t')
		{
			// TODO?: elastic tabstops
			uint_fast8_t width = tab_width - (line_i % tab_width);
			pos.x += chars[' '].x_offset * width;
			line_i += width;
			continue;
		}
		++line_i;

		Character& ch = get_char(c);

		float xpos = static_cast<float>(pos.x + ch.bearing.x);
		float ypos = static_cast<float>(pos.y + (chars['H'].bearing.y - ch.bearing.y));

		const float w = ch.size.x;
		const float h = ch.size.y;
		const float y1 = ch.flip ? 1.0f : 0.0f;
		const float y2 = 1.0f - ch.flip;
		const float vertices[] =
		{
			xpos,     ypos + h, 0.0f, y2,
			xpos,     ypos,     0.0f, y1,
			xpos + w, ypos,     1.0f, y1,

			xpos,     ypos + h, 0.0f, y2,
			xpos + w, ypos,     1.0f, y1,
			xpos + w, ypos + h, 1.0f, y2,
		};

		glBindTexture(GL_TEXTURE_2D, ch.texture);

		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		pos.x += ch.x_offset;
	}
}

text_thingy::Character& text_thingy::get_char(const char32_t c)
{
	auto i = chars.find(c);
	if(i == chars.cend())
	{
		chars.emplace(c, load_char(face, c));
		i = chars.find(c);
	}
	return i->second;
}

text_thingy::Character load_char(const FT_Face& face, const FT_ULong c)
{
	if(FT_Load_Char(face, c, FT_LOAD_RENDER))
	{
		cerr << "failed to load character: " << c << "\n";
		return
		{
			0,
			{0, 0},
			{0, 0},
			0,
			false,
		};
	}

	const auto& bitmap = face->glyph->bitmap;

	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D
	(
		GL_TEXTURE_2D,
		0,
		GL_RED,
		bitmap.width,
		bitmap.rows,
		0,
		GL_RED,
		GL_UNSIGNED_BYTE,
		bitmap.buffer
	);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return
	{
		texture,
		glm::ivec2(bitmap.width, bitmap.rows),
		glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
		face->glyph->advance.x / 64.0,
		bitmap.pitch < 0,
	};
}

#include <codecvt>
#include <locale>

u32string utf8_to_utf32(const string& s)
{
	#if defined(_MSC_VER) && _MSC_VER == 1900 // a bug prevents linking
	static std::wstring_convert<std::codecvt_utf8<int32_t>, int32_t> convert;
	auto s2 = convert.from_bytes(s);
	return u32string(reinterpret_cast<const char32_t*>(s2.data()), s2.size());
	#else
	static std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> convert;
	return convert.from_bytes(s);
	#endif
}
